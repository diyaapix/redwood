Rails.application.routes.draw do
  scope "/api/v1" do
    get    'companies',     to: 'company#list'
    get    'companies/:id', to: 'company#get'
    post   'companies',     to: 'company#create'
    patch  'companies/:id', to: 'company#update'
    delete 'companies/:id', to: 'company#delete'

    get 'companies/-/branches/:branch_id', to: 'branch#get'
    get 'companies/-/branches', to: 'branch#list'
    get 'companies/:company_id/branches', to: 'branch#list'
    post 'companies/:company_id/branches', to: 'branch#create'
    patch 'companies/-/branches/:branch_id', to: 'branch#update'
    delete 'companies/-/branches/:branch_id', to: 'branch#delete'

    get 'companies/-/branches/-/employees/:employee_id', to: 'employee#get'
    get 'companies/-/branches/-/employees', to: 'employee#list'
    get 'companies/-/branches/:branch_id/employees', to: 'employee#list'
    post 'companies/-/branches/:branch_id/employees', to: 'employee#create'
    patch 'companies/-/branches/-/employees/:employee_id', to: 'employee#update'
    delete 'companies/-/branches/-/employees/:employee_id', to: 'employee#delete'
  end
end
