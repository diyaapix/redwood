require 'rails_helper'

RSpec.describe Branch, type: :model do

  subject {
    branch = described_class.new( name: "VOX" )
    branch.name = "VOX"
    branch.company_id = create(:company).id
    branch
  } 

  after(:example){
    I18n.locale = I18n.default_locale
  }
  
  it "is valid with expected attributes being filled" do
    expect(subject).to be_valid
  end

  it "is not valid without name" do 
     subject.name = nil
     expect(subject).to_not be_valid
  end

  it "is not valid without parent company" do
     branch = Branch.new( name: "VOX" )
     expect(branch).to_not be_valid
  end

  context "translations" do
    it 'retruns correct name translation based on defined locale' do
      I18n.locale = "en" 
      expect(subject.name).to eq("VOX")

      I18n.locale = "ar" 
      expect(subject.name).to eq(nil)

      subject.name = "ar-title"
      expect(subject.name).to eq("ar-title")

      I18n.locale = "en" 
      expect(subject.name).to eq("VOX")
    end

  end
end
