require 'rails_helper'

RSpec.describe Company, type: :model do
  
  subject {
    described_class.new( name: "VOX")
  } 

  it "is valid with expected attributes being filled" do
    expect(subject).to be_valid
  end

  it "is not valid without name" do 
     subject.name = nil
     expect(subject).to_not be_valid
  end

  it "is not valid without branches_count" do 
     subject.branches_count = nil
     expect(subject).to_not be_valid
  end

  it "branches count must be 0 on creation" do
     expect(subject.branches_count).to eq(0)

     subject.branches_count = 100
     expect(subject).to_not be_valid
  end

  it "branches count must be greater than or equal 0" do
     subject.branches_count = -1
     expect(subject).to_not be_valid
  end

  it "branches count must be numerical" do
     subject.branches_count = 'Test'
     expect(subject).to_not be_valid
  end

end
