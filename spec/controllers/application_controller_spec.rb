require 'rails_helper'

RSpec.describe ApplicationController, :type => :controller do

  #as per controller implementation #I18n.with_locale will set only the locale for calle action
  controller do
    def index 
      render json:{ locale: I18n.locale }, status: 200
    end
  end

  context "locale lanuage" do
    specify "locale is set based on Accept-Language HTTP Header" do
      request.headers["HTTP_ACCEPT_LANGUAGE"] = 'ar'

      get :index
      parsed = JSON.parse(response.body)
      expect(parsed).to include("locale" => 'ar')

      request.headers["HTTP_ACCEPT_LANGUAGE"] = 'en'

      get :index
      parsed = JSON.parse(response.body)
      expect(parsed).to include("locale" => 'en')
    end

    specify "allows only :en & :ar" do
      request.headers["HTTP_ACCEPT_LANGUAGE"] = "fr"
      get :index
      expect(response).to have_http_status(400)
    end

    specify "default is :en" do
      request.headers["HTTP_ACCEPT_LANGUAGE"] = nil
      get :index
      parsed = JSON.parse(response.body)
      expect(parsed).to include("locale" => 'en')
    end
  
  end
end
