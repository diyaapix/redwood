FactoryBot.define do
  factory :employee do
    branch
    name_i18n { {"en" => "en-text"} }
    trait :with_translation do
      name_i18n { {"en" => "en-text", "ar" => "ar-text"} }
    end
  end
end
