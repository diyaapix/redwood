FactoryBot.define do
  factory :company do
    name { Faker::Name.name }
    branches_count { 0 }

    trait :with_branches do
      after(:create) do |company|
        create(:branch, company_id: company.id) 
        create(:branch, company_id: company.id) 
      end
    end
  end
end

