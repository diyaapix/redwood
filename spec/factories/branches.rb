FactoryBot.define do
  factory :branch do
    company
    name_i18n { {"en" => "en-text"} }
    trait :with_translation do
      name_i18n { {"en" => "en-text", "ar" => "ar-text"} }
    end


    trait :with_employees do
      after(:create) do |branch|
        create(:employee, branch_id: branch.id) 
        create(:employee, branch_id: branch.id) 
      end
    end
  end
end
