require 'spec_helper'

describe "localizable concern" do
  let(:test_class) { Struct.new(:title_translations) {
    include Localizable
    localizable :title, :title_translations
  }}

  let(:localizable_obj) { test_class.new({ "en" => "Test", "ar" => "Tajrba"})}

  it "return the localized value based on current locale" do
    I18n.locale = "en" 
    expect(localizable_obj.title).to eq("Test")

    I18n.locale = "ar" 
    expect(localizable_obj.title).to eq("Tajrba")
  end

  it "does not crash when the translation object is nil" do
    I18n.locale = "ar" 
    localizable_obj.title_translations = nil
    expect(localizable_obj.title).to eq(nil)
  end

  it "does not crash when the translation object is {}" do
    I18n.locale = "en" 
    localizable_obj.title_translations = nil
    expect(localizable_obj.title).to eq(nil)
  end

  #TODO: Handle cases when the hstore is faluty
end
