require 'rails_helper'

RSpec.describe "Employees", type: :request do

  let (:valid_attributes) {
    {
      name: Faker::Name.name,
    }
  }

  let(:invalid_attributes) {
    {
      name: nil,
    }
  }

  before(:context) {
    DatabaseCleaner.clean_with(:truncation)
    @branch1 = create(:branch, :with_employees)
    @branch2 = create(:branch, :with_employees)
    @employee = @branch1.employees.first
    @employee_trans = create(:employee, :with_translation)
  }

  describe "GET /companies/-/branches/-/employees/:employee_id" do

    context "record exist" do
      it "returns a successful response" do
        get "/api/v1/companies/-/branches/-/employees/#{@employee.id}"
        expect(response).to have_http_status(200)
      end

      it "returns employee details" do
        get "/api/v1/companies/-/branches/-/employees/#{@employee.id}"
        parsed = JSON.parse(response.body)
        expect(parsed).to include("id" => @branch1.id)
      end

      it "returns correct translation when lang header is provided" do
        get "/api/v1/companies/-/branches/-/employees/#{@employee_trans.id}", headers: {HTTP_ACCEPT_LANGUAGE: 'ar'}

        parsed = JSON.parse(response.body)
        expect(parsed).to include("name" => 'ar-text')

        get "/api/v1/companies/-/branches/-/employees/#{@employee_trans.id}", headers: {HTTP_ACCEPT_LANGUAGE: 'en'}

        parsed = JSON.parse(response.body)
        expect(parsed).to include("name" => 'en-text')
      end
    end

    context "record does not exist" do
      it "returns error status 404" do
        get "/api/v1/companies/-/branches/-/employees/99"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "GET /companies/-/branches/-/employees" do
    it "returns a successful response" do
      get "/api/v1/companies/-/branches/-/employees"
      expect(response).to have_http_status(200)
    end

    it "returns all branches" do
      get "/api/v1/companies/-/branches/-/employees"
      parsed = JSON.parse(response.body)
      expect(parsed).to include(
        a_hash_including("id" => @employee.id),
        a_hash_including("id" => @employee_trans.id)
      )
    end
  end

  describe "GET /companies/-/branches/-/employees" do
    it "returns a successful response" do
      get "/api/v1/companies/-/branches/-/employees"
      expect(response).to have_http_status(200)
    end

    it "returns all branches" do
      get "/api/v1/companies/-/branches/-/employees"
      parsed = JSON.parse(response.body)
      expect(parsed).to include(
        a_hash_including("id" => @employee.id),
        a_hash_including("id" => @employee_trans.id)
      )
    end
  end


  describe "GET /companies/-/branches/:branch_id/employees" do
    context "record exist" do
      it "returns branche employees" do
        get "/api/v1/companies/-/branches/#{@branch1.id}/employees"
        parsed = JSON.parse(response.body)
        expect(parsed.count).to eq(2)
        expect(parsed).to include(
          a_hash_including("id" => @branch1.id)
        )
      end
    end
    context "record does not exist" do
      it "returns error status 404" do
        get "/api/v1/companies/-/branches/99/employees"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "POST /companies/-/branches/:branch_id/employees" do
    context "with valid data" do
      it "returns a successful status 201" do
        post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: valid_attributes
        expect(response).to have_http_status(201)
      end
      it "creates an employee with it's translations" do
        post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: valid_attributes
        parsed = JSON.parse(response.body)
        expect(parsed).to include('id' => a_kind_of(Integer))
      end
      specify "created employee must belong to the correct branch " do
        post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: valid_attributes
        parsed = JSON.parse(response.body)
        expect(Branch.find_by('id': @branch1.id).employees.pluck(:id)).to include( parsed["id"])
      end
    end

    context "with invalid data" do
      it "does not create a new employee" do
        expect {
          post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: invalid_attributes
        }.to change(Branch, :count).by(0)
      end
      it "returns error message for each invalid element" do
        post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: invalid_attributes
        parsed = JSON.parse(response.body)
        errors_list = parsed["errors"];
        expect(errors_list).to have_key("name")
      end
      it "returns error status 422" do
        post "/api/v1/companies/-/branches/#{@branch1.id}/employees", params: invalid_attributes
        parsed = JSON.parse(response.body)
        expect(response).to have_http_status(422)
      end
    end

    context "branch doesn't exist" do
      it "returns error status 404" do
        post "/api/v1/companies/-/branches/99/employees"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "PATCH /companies/-/branches/-/employees/:employee_id" do
    context "with valid data" do
      it "returns a successful status 200" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: valid_attributes
        expect(response).to have_http_status(200)
      end
      it "updates record data" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: valid_attributes
        expect(Employee.find(@employee.id).name).to eq(valid_attributes[:name])
      end
    end
    context "with invalid data" do
      it "does not update the employee" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: invalid_attributes
        expect(Employee.find(@employee.id).name).to eq(@employee.name)
      end
      it "returns error status 422" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: invalid_attributes
        expect(response).to have_http_status(422)
      end
      it "returns error message for each invalid element" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: invalid_attributes
        parsed = JSON.parse(response.body)
        errors_list = parsed["errors"];
        expect(errors_list).to have_key("name")
      end
    end
    context "record doesn't exist" do
      it "returns error status 404" do
        patch "/api/v1/companies/-/branches/-/employees/#{@employee.id}", params: valid_attributes
      end
    end
  end

  describe "DELETE /companies/-/branches/-/employees/:employee_id" do
    context "record exist" do
      it "deletes the branch" do
        expect {
          delete "/api/v1/companies/-/branches/-/employees/#{@employee.id}"
          expect(response).to have_http_status(200)
        }.to change(Employee, :count).by(-1)
      end
    end

    context "record doesn't exist" do
      it "returns error status 404" do
        delete "/api/v1/companies/-/branches/-/employees/99"
        expect(response).to have_http_status(404)
      end
    end
  end
end
