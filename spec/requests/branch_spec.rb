require 'rails_helper'

RSpec.describe "Branches", type: :request do

  let (:valid_attributes) {
    {
      name: Faker::Name.name,
    }
  }

  let(:invalid_attributes) {
    {
      name: nil,
    }
  }

  before(:context) {
    DatabaseCleaner.clean_with(:truncation)
    @company1 = create(:company, :with_branches)
    @company2 = create(:company, :with_branches)
    @branch1 = @company1.branches.first
    @branch_trans = create(:branch, :with_translation)
  }

  describe "GET /companies/-/branches/:id" do

    context "record exist" do
      it "returns a successful response" do
        get "/api/v1/companies/-/branches/#{@branch1.id}"
        expect(response).to have_http_status(200)
      end

      it "returns company details" do
        get "/api/v1/companies/-/branches/#{@branch1.id}"
        parsed = JSON.parse(response.body)
        expect(parsed).to include("id" => @branch1.id)
      end

      it "returns correct translation when lang header is provided" do
        get "/api/v1/companies/-/branches/#{@branch_trans.id}", headers: {HTTP_ACCEPT_LANGUAGE: 'ar'}
        parsed = JSON.parse(response.body)
        expect(parsed).to include("name" => 'ar-text')

        get "/api/v1/companies/-/branches/#{@branch_trans.id}", headers: {HTTP_ACCEPT_LANGUAGE: 'en'}
        parsed = JSON.parse(response.body)
        expect(parsed).to include("name" => 'en-text')
      end
    end
    context "record does not exist" do
      it "returns error status 404" do
        get "/api/v1/companies/-/branches/9999" 
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "GET /companies/-/branches" do
    it "returns a successful response" do
      get "/api/v1/companies/-/branches"
      expect(response).to have_http_status(200)
    end

    it "returns all branches" do
      get "/api/v1/companies/-/branches"
      parsed = JSON.parse(response.body)
      expect(parsed).to include(
        a_hash_including("id" => @branch1.id),
        a_hash_including("id" => @branch_trans.id)
      )
    end
  end

  describe "GET /companies/:company_id/branches" do

    context "record exist" do
      it "returns company branches" do
        get "/api/v1/companies/#{@company1.id}/branches"
        parsed = JSON.parse(response.body)
        expect(parsed.count).to eq(2)
        expect(parsed).to include(
          a_hash_including("id" => @branch1.id)
        )
      end
    end
    context "record does not exist" do
      it "returns error status 404" do
        get "/api/v1/companies/99/branches"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "POST /companies/:company_id/branches" do
    context "with valid data" do
      it "returns a successful status 201" do
        post "/api/v1/companies/#{@company1.id}/branches", params: valid_attributes
        expect(response).to have_http_status(201)
      end

      it "creates the branch with it's translations" do
        post "/api/v1/companies/#{@company1.id}/branches", params: valid_attributes
        parsed = JSON.parse(response.body)
        expect(parsed).to include('id' => a_kind_of(Integer))
      end

      specify "created branch must belong to the correct company " do
        post "/api/v1/companies/#{@company1.id}/branches", params: valid_attributes
        parsed = JSON.parse(response.body)
        expect(Company.find_by('id': @company1.id).branches.pluck(:id)).to include( parsed["id"])
      end

      specify "related company branches should be incrumented by 1", :observers do
        fcompany = create(:company);
        expect(fcompany.branches_count).to eq(0)
        post "/api/v1/companies/#{fcompany.id}/branches", params: valid_attributes
        fcompany.reload
        expect(fcompany.branches_count).to eq(1)
      end
    end

    context "with invalid data" do
      it "does not create a new branch" do
        expect {
          post "/api/v1/companies/#{@company1.id}/branches", params: invalid_attributes
        }.to change(Branch, :count).by(0)
      end

      it "returns error message for each invalid element" do
        post "/api/v1/companies/#{@company1.id}/branches", params: invalid_attributes
        parsed = JSON.parse(response.body)
        errors_list = parsed["errors"];
        expect(errors_list).to have_key("name")
      end

      it "returns error status 422" do
        post "/api/v1/companies/#{@company1.id}/branches", params: invalid_attributes
        expect(response).to have_http_status(422)
      end
    end

    context "company doesn't exist" do
      it "returns error status 404" do
        post "/api/v1/companies/99/branches", params: invalid_attributes
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "PATCH /companies/-/branches/:branch_id" do

    context "with valid data" do
      it "returns a successful status 200" do
        patch "/api/v1/companies/-/branches/#{@branch1.id}", params: valid_attributes
        expect(response).to have_http_status(200)
      end

      it "updates record data" do
        patch "/api/v1/companies/-/branches/#{@branch1.id}", params: valid_attributes
        expect(Branch.find(@branch1.id).name).to eq(valid_attributes[:name])
      end
    end

    context "with invalid data" do
      it "does not update the branch" do
        patch "/api/v1/companies/-/branches/#{@branch1.id}", params: invalid_attributes
        expect(Branch.find(@branch1.id).name).to eq(@branch1.name)
      end

      it "returns error status 422" do
        patch "/api/v1/companies/-/branches/#{@branch1.id}", params: invalid_attributes
        expect(response).to have_http_status(422)
      end

      it "returns error message for each invalid element" do
        patch "/api/v1/companies/-/branches/#{@branch1.id}", params: invalid_attributes
        parsed = JSON.parse(response.body)
        errors_list = parsed["errors"];
        expect(errors_list).to have_key("name")
      end
    end

    context "record doesn't exist" do
      it "returns error status 404" do
        patch "/api/v1/companies/-/branches/99"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "DELETE /companies/-/branches/:branch_id" do
    context "record exist" do
      it "deletes the branch" do
        expect {
          delete "/api/v1/companies/-/branches/#{@branch1.id}"
          expect(response).to have_http_status(200)
        }.to change(Branch, :count).by(-1)
      end
      specify "related company branches count should be decremented by 1", :observers do
        fcompany = create(:company, :with_branches)
        current_count = fcompany.reload.branches.count
        delete "/api/v1/companies/-/branches/#{fcompany.branches.first.id}"
        fcompany.reload
        expect(fcompany.branches_count).to eq(current_count - 1)
      end
    end

    context "record doesn't exist" do
      it "returns error status 404" do
        delete "/api/v1/companies/-/branches/99"
        expect(response).to have_http_status(404)
      end
    end
  end

end
