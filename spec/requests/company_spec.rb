require 'rails_helper'

#This will contain happy scenarios
RSpec.describe "/companies", type: :request do

  let (:valid_attributes) {
    { name: Faker::Name.name }
  }

  let(:invalid_attributes) {
    { name: nil }
  }

  before(:context) {
    DatabaseCleaner.clean_with(:truncation)
    @company1 = create(:company)
    @company2 = create(:company)
  }

  describe "GET /companies" do

    it 'Returns a successful response' do
      get '/api/v1/companies'
      expect(response).to have_http_status(200)
    end

    it 'Retruns all companies' do
      get '/api/v1/companies'
      parsed = JSON.parse(response.body)
      expect(parsed).to match([
        a_hash_including("name" => @company1.name),
        a_hash_including("name" => @company2.name)
      ])
    end
  end

  describe "GET /companies/:id" do
    context "record exist" do
      it "returns a successful response" do
        get "/api/v1/companies/#{@company1.id}"
        expect(response).to have_http_status(200)
      end

      it "returns the requested record" do
        get "/api/v1/companies/#{@company1.id}"

        parsed = JSON.parse(response.body)
        expect(parsed).to include("id" => @company1.id)
      end
    end
    context "record does not exist" do
      it "returns error status 404" do
        get "/api/v1/companies/99"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "POST /companies" do
    context "with valid parameters " do
      it "returns a successful status 201" do
        post '/api/v1/companies', params: valid_attributes
        expect(response).to have_http_status(201)
      end

      it "creates a new Company" do
        post '/api/v1/companies', params: valid_attributes

        parsed = JSON.parse(response.body)
        expect(parsed).to include('id' => a_kind_of(Integer))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Company" do
        expect {
          post '/api/v1/companies', params: invalid_attributes
        }.to change(Company, :count).by(0)
      end

      it "returns error message for each invalid element" do
          post '/api/v1/companies', params: invalid_attributes
          parsed = JSON.parse(response.body)
          errors_list = parsed["errors"];
          expect(errors_list).to have_key("name")
      end

      it "returns error status 422" do
        post '/api/v1/companies', params: invalid_attributes
        expect(response).to have_http_status(422)
      end
    end
  end

  describe "PATCH /companies/:id" do
    context "Company exist" do
      context "with valid parameters" do
        it "updates Company details " do
          company = { id: @company1.id, name: "NEW_NAME"}
          patch "/api/v1/companies/#{@company1.id}", params: company
        end
      end

      context "with invalid parameters" do
        it "returns error message for each invalid element" do
            company = { id: @company2.id, name: nil}
            patch "/api/v1/companies/#{@company2.id}", params: company

            parsed = JSON.parse(response.body)
            errors_list = parsed["errors"];
            expect(errors_list).to have_key("name")
        end

        it "returns error status 422" do
          company = { id: @company2.id, name: nil}
          patch "/api/v1/companies/#{@company2.id}", params: company
          expect(response).to have_http_status(422)
        end
      end
    end

    context "Company does not exist" do
      it "returns error status 404" do
        patch "/api/v1/companies/99"
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "DELETE /companies/:id" do
    context "Company exist" do
      it "deletes the company" do
        expect {
          delete "/api/v1/companies/#{@company1.id}"
        }.to change(Company, :count).by(-1)
      end
    end

    context "Company does not exist" do
      it "returns error status 404" do
        delete "/api/v1/companies/99"
        expect(response).to have_http_status(404)
      end
    end
  end

end
