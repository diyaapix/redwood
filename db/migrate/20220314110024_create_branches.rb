class CreateBranches < ActiveRecord::Migration[7.0]
  enable_extension 'hstore' unless extension_enabled?('hstore')

  def change
    create_table :branches do |t|
      t.belongs_to :company, foreign_key: true
      t.hstore 'name_i18n' 
      t.timestamps
    end
  end
end
