class CreateEmployees < ActiveRecord::Migration[7.0]
  def change
    create_table :employees do |t|
      t.belongs_to :branch, foreign_key: true
      t.hstore 'name_i18n'
      t.timestamps
    end
  end
end
