class CreateCompanies < ActiveRecord::Migration[7.0]
  def change
    create_table :companies do |t|
      t.string :name, null:false
      t.integer :branches_count, default:0, null:false
      t.integer :lock_version
      t.timestamps
    end
  end
end
