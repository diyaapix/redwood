class Company < ApplicationRecord
  has_many :branches

  validates :name, presence:true
  validates :branches_count, presence:true, numericality: true, comparison: { greater_than_or_equal_to: 0}
  validates :branches_count, comparison: { equal_to: 0 }, on: :create
end
