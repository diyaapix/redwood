module Localizable
  extend ActiveSupport::Concern

  included do
    def self.localizable(attr_name, hstore_name)

      attribute attr_name if defined?(attribute)

      define_method attr_name do
        locale = I18n.locale
        hstore = send(hstore_name) || {}
        t = hstore[locale.to_s] || nil 
      end

      define_method "#{attr_name}=" do |value|
        locale = I18n.locale
        default_locale = I18n.default_locale

        send("#{hstore_name}=", {}) unless send("#{hstore_name}")

        hstore = send("#{hstore_name}")
        hstore[locale.to_s] = value
        hstore[default_locale.to_s] = value unless hstore[default_locale.to_s]

        return value
      end
    end
  end
end
