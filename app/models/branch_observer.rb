class BranchObserver < ActiveRecord::Observer
  def after_create(branch)
    #Get new company object with latest lock_version
    company = Company.find(branch.company_id)

    company.branches_count += 1;
    company.save
  end
  def after_destroy(branch)
    #Get new company object with latest lock_version
    company = Company.find(branch.company_id)
    company.branches_count -= 1;
    company.save
  end
end
