class Branch < ApplicationRecord
  include Localizable

  localizable :name, :name_i18n
  validates :name, presence:true
  belongs_to :company
  has_many :employees
end
