class ApplicationController < ActionController::API
  around_action :switch_locale

  rescue_from I18n::InvalidLocale, :with => :handle_invalid_locale

  #TODO: More robust implementation is need
  def switch_locale(&action)
    locale = extract_locale_from_accept_language_header || I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  def extract_locale_from_accept_language_header
    header = request.env['HTTP_ACCEPT_LANGUAGE'];
    if header
      header.scan(/^[a-z]{2}/).first
    else
      nil
    end
  end

  def handle_invalid_locale
    render json: {message: "Invalid locale"}, status: 400
  end
end
