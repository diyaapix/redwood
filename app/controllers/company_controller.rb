class CompanyController < ApplicationController

  def list
    companies = Company.all
    render json: companies
  end

  def get
    company = Company.find_by(id: params[:id])

    unless company then 
      render json: nil, status: 404
    else
      render json: company
    end
  end

  def create
    company = Company.new do |c|
      c.name = params[:name]
    end
    if company.save
      render json: company, status: 201
    else
      render json: { errors: company.errors.messages }, status: 422
    end
  end

  def update
    company = Company.find_by(id: params[:id])

    if !company
      render json: nil, status: 404
      return
    end
    
    permitted = params.permit(:name)
    company.update(permitted)

    if company.valid?
      render json: company, status: 200
    else
      render json: { errors: company.errors.messages }, status: 422
    end
  end

  def delete
    company = Company.find_by(id: params[:id])
    if company
      company.destroy
      render json: nil, status: 200
    else
      render json: nil, status: 404
    end
  end
end
