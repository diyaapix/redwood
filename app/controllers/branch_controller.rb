class BranchController < ApplicationController
  def get()
    branch = Branch.find_by(id: params[:branch_id])

    unless branch then 
      render json: nil, status: 404
    else
      render json: branch 
    end
  end

  def list()
    branches = []

    if params[:company_id]
      company = Company.find_by(id: params[:company_id]) 
      if !company
        render json: nil, status: 404
        return
      end
      branches = company.branches
    else
      branches = Branch.all
    end

    render json: branches
  end


  def create
    company = Company.find_by(id: params[:company_id]) 

    if !company
      render json: nil, status: 404
      return
    end

    branch = company.branches.create(
      name: params[:name]
    )

    if branch.persisted?
      render json: branch, status: 201
    else
      render json: { errors: branch.errors.messages }, status: 422
    end
  end

  def update 
    branch = Branch.find_by(id: params[:branch_id]) 

    if !branch
      render json: nil, status: 404
      return
    end

    permitted = params.permit(:name)
    branch.update(permitted)

    if branch.valid?
      render json: branch, status:200
    else
      render json: { errors: branch.errors.messages }, status: 422
    end
  end

  def delete
    branch = Branch.find_by(id: params[:branch_id])
    if branch
      branch.destroy
      render json: nil, status: 200
    else
      render json: nil, status: 404
    end
  end

end
