class EmployeeController < ApplicationController
  def get 
    employee = Employee.find_by(id: params[:employee_id])

    unless employee then 
      render json: nil, status: 404
    else
      render json: employee 
    end
  end

  def list
    employees = []

    if params[:branch_id]
      company = Branch.find_by(id: params[:branch_id]) 
      if !company
        render json: nil, status: 404
        return
      end
      employees = company.employees
    else
      employees = Employee.all
    end

    render json: employees
  end
  def create
    branch = Branch.find_by(id: params[:branch_id]) 

    if !branch
      render json: nil, status: 404
      return
    end

    employee = branch.employees.create(
      name: params[:name]
    )

    if employee.persisted?
      render json: employee, status: 201
    else
      render json: { errors: employee.errors.messages }, status: 422
    end
  end

  def update 
    employee = Employee.find_by(id: params[:employee_id]) 

    if !employee
      render json: nil, status: 404
      return
    end

    permitted = params.permit(:name)
    employee.update(permitted)

    if employee.valid?
      render json: employee, status:200
    else
      render json: { errors: employee.errors.messages }, status: 422
    end
  end

  def delete
    employee = Employee.find_by(id: params[:employee_id])
    if employee
      employee.destroy
      render json: nil, status: 200
    else
      render json: nil, status: 404
    end
  end
end

